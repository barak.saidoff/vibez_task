/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React, { useState } from "react";

// Chakra imports
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Icon,
  Image,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  SimpleGrid,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
// Assets
import { NavLink } from "react-router-dom";
import { MdOutlineRemoveRedEye } from "react-icons/md";
import { RiEyeCloseLine } from "react-icons/ri";
import { useSignup } from "../../../../hooks/useSignUp";
import logo from "assets/svg/NewTwoLine.png";
import PhoneField from "components/fields/PhoneField";

function SignUpForm({ setSuccess }) {
  // Chakra color mode
  const textColor = useColorModeValue("navy.700", "white");
  const textColorSecondary = "gray.400";
  const textColorDetails = useColorModeValue("navy.700", "secondaryGray.600");
  const textColorBrand = useColorModeValue("brand.500", "white");
  const brandStars = useColorModeValue("brand.500", "brand.400");
  const [show, setShow] = React.useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [errorMsg, setErrorMsg] = useState("");
  const [isChecked, setIsChecked] = useState(false);
  const { signup, isLoading, error } = useSignup();

  const handleClick = () => setShow(!show);
  const handleSignUp = async (e) => {
    if (!isChecked) {
      setErrorMsg("Please read and accept Terms of Service and Privacy Policy");
      return;
    }
    setErrorMsg("");
    e.preventDefault();
    if (password != password2) {
      setErrorMsg("Passwords don't match");
      return;
    }
    let res = await signup(firstName, lastName, email, phone, password);
    if (res) setSuccess(true);
  };
  return (
    <>
      {/* <Box me="auto">
        <Heading
          color={textColor}
          fontSize={{ base: "34px", lg: "36px" }}
          mb="10px"
        >
          Sign Up
        </Heading>
        <Text
          mb="36px"
          ms="4px"
          color={textColorSecondary}
          fontWeight="400"
          fontSize="md"
        >
          Enter your email and password to sign up!
        </Text>
      </Box> */}
      <Flex
        me="auto"
        w={{ base: "100%", md: "420px" }}
        justifyContent={"center"}
        maxW="100%"
        marginBottom={"30px"}
      >
        <Image src={logo} />
      </Flex>
      <Flex
        zIndex="2"
        direction="column"
        w={{ base: "100%", md: "420px" }}
        maxW="100%"
        background="transparent"
        borderRadius="15px"
        mx={{ base: "auto", lg: "unset" }}
        me="auto"
        mb={{ base: "20px", md: "auto" }}
      >
        <FormControl>
          <SimpleGrid
            columns={{ base: "1", md: "2" }}
            gap={{ sm: "10px", md: "26px" }}
          >
            <Flex direction="column">
              <FormLabel
                display="flex"
                ms="4px"
                fontSize="sm"
                fontWeight="500"
                color={textColor}
                mb="8px"
              >
                First name<Text color={brandStars}>*</Text>
              </FormLabel>
              <Input
                isRequired={true}
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                fontSize="sm"
                ms={{ base: "0px", md: "4px" }}
                placeholder="First name"
                textAlign={"center"}
                variant="auth"
                mb="24px"
                size="lg"
              />
            </Flex>
            <Flex direction="column">
              <FormLabel
                display="flex"
                ms="4px"
                fontSize="sm"
                fontWeight="500"
                color={textColor}
                mb="8px"
              >
                Last name<Text color={brandStars}>*</Text>
              </FormLabel>
              <Input
                isRequired={true}
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                variant="auth"
                fontSize="sm"
                placeholder="Last name"
                textAlign={"center"}
                mb="24px"
                size="lg"
              />
            </Flex>
          </SimpleGrid>
          <FormLabel
            display="flex"
            ms="4px"
            fontSize="sm"
            fontWeight="500"
            color={textColor}
            mb="8px"
          >
            Email<Text color={brandStars}>*</Text>
          </FormLabel>
          <Input
            isRequired={true}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            variant="auth"
            fontSize="sm"
            type="email"
            placeholder="Email"
            textAlign={"center"}
            mb="24px"
            size="lg"
          />
          <FormLabel
            display="flex"
            ms="4px"
            fontSize="sm"
            fontWeight="500"
            color={textColor}
            mb="8px"
          >
            Phone<Text color={brandStars}>*</Text>
          </FormLabel>
          <PhoneField value={phone} setValue={setPhone} />
          {/* <Input
            isRequired={true}
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            variant="auth"
            fontSize="sm"
            type="number"
            placeholder="Phone"
            textAlign={"center"}
            mb="24px"
            size="lg"
          /> */}
          <FormLabel
            ms="4px"
            fontSize="sm"
            fontWeight="500"
            isRequired={true}
            color={textColor}
            display="flex"
          >
            Password<Text color={brandStars}>*</Text>
          </FormLabel>
          <InputGroup size="md">
            <Input
              isRequired={true}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              variant="auth"
              fontSize="sm"
              ms={{ base: "0px", md: "4px" }}
              placeholder="Password"
              textAlign={"center"}
              mb="24px"
              size="lg"
              type={show ? "text" : "password"}
            />
            <InputRightElement display="flex" alignItems="center" mt="4px">
              <Icon
                color={textColorSecondary}
                _hover={{ cursor: "pointer" }}
                as={show ? RiEyeCloseLine : MdOutlineRemoveRedEye}
                onClick={handleClick}
              />
            </InputRightElement>
          </InputGroup>
          <FormLabel
            ms="4px"
            fontSize="sm"
            fontWeight="500"
            isRequired={true}
            color={textColor}
            display="flex"
          >
            Verify Password<Text color={brandStars}>*</Text>
          </FormLabel>
          <InputGroup size="md">
            <Input
              isRequired={true}
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              variant="auth"
              fontSize="sm"
              ms={{ base: "0px", md: "4px" }}
              placeholder="Verify password"
              textAlign={"center"}
              mb="24px"
              size="lg"
              type={show ? "text" : "password"}
            />
            <InputRightElement display="flex" alignItems="center" mt="4px">
              <Icon
                color={textColorSecondary}
                _hover={{ cursor: "pointer" }}
                as={show ? RiEyeCloseLine : MdOutlineRemoveRedEye}
                onClick={handleClick}
              />
            </InputRightElement>
          </InputGroup>
          <Flex justifyContent="space-between" align="center" mb="24px">
            <FormControl display="flex" alignItems="start">
              <Checkbox
                id="agreed"
                colorScheme="brandScheme"
                me="10px"
                mt="3px"
                value={isChecked}
                onChange={(e) => setIsChecked(e.target.checked)}
              />
              <FormLabel
                htmlFor="remember-login"
                mb="0"
                fontWeight="normal"
                color={textColor}
                fontSize="sm"
              >
                By creating an account means you agree to the{" "}
                <NavLink to="/auth/licenses" target="_blank">
                  Terms of Service and our Privacy Policy
                </NavLink>
              </FormLabel>
            </FormControl>
          </Flex>
          {(error || errorMsg) && (
            <Box pb="20px" w="100%" textAlign={"center"}>
              <Text color="red">
                {error}
                {errorMsg}
              </Text>
            </Box>
          )}
          <Button
            onClick={handleSignUp}
            variant="brand"
            fontSize="14px"
            fontWeight="500"
            w="100%"
            h="50"
            mb="24px"
          >
            Create my account
          </Button>
        </FormControl>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="start"
          maxW="100%"
          mt="0px"
        >
          <Text color={textColorDetails} fontWeight="400" fontSize="sm">
            Already a member?
            <NavLink to="/auth/sign-in">
              <Text color={textColorBrand} as="span" ms="5px" fontWeight="500">
                Sign in
              </Text>
            </NavLink>
          </Text>
        </Flex>
      </Flex>
    </>
  );
}

export default SignUpForm;
