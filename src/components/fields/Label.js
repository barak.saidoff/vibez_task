// Chakra imports
import {
    FormLabel,
    Text,
    useColorModeValue,
} from "@chakra-ui/react";
// Custom components
import React from "react";

export default function Label(props) {
    const { id, label, extra, required, placeholder, type, mb, ...rest } = props;
    // Chakra Color Mode
    const textColorPrimary = useColorModeValue("secondaryGray.900", "white");
    const brandStars = useColorModeValue("brand.500", "brand.400");

    return (
        <FormLabel
            display='flex'
            ms='10px'
            htmlFor={id}
            fontSize='sm'
            color={textColorPrimary}
            fontWeight='bold'
            _hover={{ cursor: "pointer" }}>
            {label}
            <Text fontSize='sm' fontWeight='400' ms='2px'>
                {extra}
            </Text>
            {
                required && <Text color={brandStars}>*</Text>
            }
        </FormLabel>
    );
}
